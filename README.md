# Captain
Captain is an advanced chroot management system or an "almost" Operating
System Level virtualization tool. It is strongly inspired by Solaris
Zones and borrows some of Docker’s terminology.

It only aims to work on NetBSD. It is not a complete container solution!
As of NetBSD 9.2, there is no equivalent to Linux namespace, Linux
cgroup or Jails in NetBSD. Which means there is no network isolation (a
ship can see all the host network interfaces*) and no process isolation
(the ship can see other processes and, if run as root, can kill other
ship processes). It only uses chroot for isolation at the filesystem level.

In captain terns, a ship is a complete chroot or ,if you prefer, a
wannabe container. A hull is a template consisting in a base NetBSD
system with or without extras (pre-installed software for instance)
similar to a docker image.

This concept is similar to a docker image/container but we do not share
the philosophy of a single process per container. Multiple process can
be run inside a ship just as in a FreeBSD Jail or Solaris Zone.

Usage :

```
SHIP
usage: bin/captain.sh ship start <ship_name>
usage: bin/captain.sh ship stop <ship_name>
usage: bin/captain.sh ship build <ship_name>
usage: bin/captain.sh ship delete <ship_name>
usage: bin/captain.sh ship destroy <ship_name>
usage: bin/captain.sh ship blueprint <ship_name>
usage: bin/captain.sh ship ls|list (short list)
usage: bin/captain.sh ship ll (long list)
usage: bin/captain.sh ship reset-running

HULL
usage: bin/captain.sh hull blueprint <hull_name>
usage: bin/captain.sh hull build <hull_name>
usage: bin/captain.sh hull destroy <hull_name>
usage: bin/captain.sh hull delete <blueprint_name>
usage: bin/captain.sh hull download-sets
usage: bin/captain.sh hull flush-sets
usage: bin/captain.sh hull ls|list
```

Get started :

( Optionnal ) Download the "native" sets :

```
demo-host# bin/captain.sh hull download-sets
Assuming native NetBSD Version 9.1
Created /srv/captain/var/captain/cache/NetBSD-9.1/amd64
Downloading base.tar.xz from ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/amd64/binary/sets
...
```

( Optionnal ) Download sets for a wanted release :
```
demo-host# bin/captain.sh hull download-sets 9.2
Created /srv/captain/var/captain/cache/NetBSD-9.2/amd64
Downloading base.tar.xz from ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.2/amd64/binary/sets
...
```

Create your firts hull :

```
demo-host# bin/captain.sh hull blueprint
Enter hull name followed by [ENTER]: demo-hull-9.2
Enter hull path followed by [ENTER] (default /srv/captain/var/captain/hulls/demo-hull-9.2):
Trying to create /srv/captain/var/captain/hulls/demo-hull-9.2
Created /srv/captain/var/captain/hulls/demo-hull-9.2
Enter NetBSD version followed by [ENTER]: 9.2
Enter NetBSD Installation type
Choices are :
1- Minimal
2- Full Without X
3- Full With X
Enter NetBSD Installation type followed by [ENTER]: 2

demo-host# bin/captain.sh hull build demo-hull-9.2
Building hull demo-hull-9.2
Created /srv/captain/var/captain/hulls/demo-hull-9.2
Installing base
...
```

Create your first ship :
```
demo-host# bin/captain.sh ship blueprint
Enter ship name followed by [ENTER]: demo-ship-9.2
Enter ship path followed by [ENTER] (default /srv/captain/var/captain/ships/demo-ship-9.2):
Trying to create /srv/captain/var/captain/ships/demo-ship-9.2
Created /srv/captain/var/captain/ships/demo-ship-9.2
From which hull do you want your ship built?
Available Hulls :
--------------------------------------------------------------------------------------------------
|NAME             PATH                                     VERS     INSTALL_TYPE     STATUS      |
--------------------------------------------------------------------------------------------------
|nb91min          /srv/captain/var/captain/hulls/nb91min   9.1      Minimal          installed   |
|nb91fwx          /srv/captain/var/captain/hulls/nb91fwx   9.1      FullWithX        installed   |
|nb82min          /srv/captain/var/captain/hulls/nb82min   8.2      Minimal          installed   |
|nb91fnx          /srv/captain/var/captain/hulls/nb91fnx   9.1      FullNoX          installed   |
|demo-hull-9.2    /srv/captain/var/captain/hulls/demo-hull 9.2      FullNoX          configured  |
--------------------------------------------------------------------------------------------------
Enter hull name followed by [ENTER] demo-hull-9.2
What IP should the ship have?
Enter IP followed by [ENTER]: 192.168.1.253
192.168.1.253
What net interface should the ship use?
Here is the list of interfaces :
wm0
lo0
Enter interface followed by [ENTER]: wm0

demo-host# bin/captain.sh ship build demo-ship-9.2
Building ship demo-ship-9.2
...
```

List ships :
```
demo-host# bin/captain.sh ship ls
---------------------------------------------------------
|NAME             NET      IP               STATUS      |
---------------------------------------------------------
|test1            wm0      192.168.1.231    installed   |
|2020Q3-2         wm0      192.168.1.131    running     |
|demo-ship-9.2    wm0      192.168.1.253    configured  |
---------------------------------------------------------
demo-host# bin/captain.sh ship ll
-----------------------------------------------------------------------------------------------------------------------------------------------
|NAME             PATH                                       HULL             VERS     INSTALL_TYPE     NET      IP               STATUS      |
-----------------------------------------------------------------------------------------------------------------------------------------------
|test1            /srv/captain/var/captain/ships/test1       nb91min          9.1      Minimal          wm0      192.168.1.231    installed   |
|2020Q3-2         /srv/captain/var/captain/ships/2020Q3-2    nb91fnx          9.1      FullNoX          wm0      192.168.1.131    running     |
|demo-ship-9.2    /srv/captain/var/captain/ships/demo-ship-9 demo-hull-9.2    9.2      FullNoX          wm0      192.168.1.253    configured  |
-----------------------------------------------------------------------------------------------------------------------------------------------
```
Start a ship :

```
demo-host# bin/captain.sh ship start test1
Addind 192.168.1.231 to wm0
Mounting specials
mounting /srv/captain/var/captain/ships/test1/dev/pts/
```

Enter a ship :
```
demo-host# bin/captain.sh ship board test1
Chrooting in test1
This ship has this ip : 192.168.1.231
Remember : NetBSD has no network isolation. It is your responsibility to make sure your services listen to this IP

```

Stop a ship :
```
demo-host# bin/captain.sh ship stop test1
Removing 192.168.1.231 to wm0
Umounting specials
/srv/captain/var/captain/ships/test1/dev/pts/ already umounted
```

(Advanced) Reset all running state to installed. Captain store its ship status in a flat file. In the event of a power outage, the next reboot, captain will show its ship still running. This option is a quick way to fix this
```
demo-host# ./bin/captain ship reset-running
Are you sure you want to reset all ship status from running to installed ? (Usually mean there has been a power outage or the host crashed) y
== Changing ship1 status to installed ==
== Changing ship4 status to installed ==
== Changing ship6 status to installed ==

```

