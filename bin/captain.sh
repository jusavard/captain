#!/bin/ksh
# include
BIN_DIR=$(dirname "$0")
CONF_DIR=$(cd $BIN_DIR/../etc;pwd)
CACHE_DIR=$(cd $BIN_DIR/../var/captain/cache;pwd)
ARCHITECTURE="$(uname -m)"
OS_NAME="$(uname -s)"
HULL_NAME=""
HULL_INSTALL_PATH=""
HULL_INSTALL_PATH_DEFAULT="$(cd $BIN_DIR/../var/captain/hulls;pwd)"
HULL_STATUS_FILE="$CONF_DIR/hull_status"
HULL_STATUS=""
HULL_INSTALL_TYPE=""
HULL_NETBSD_VERSION=""
HULL_NAMES_LIST=""
SHIP_PATH_DEFAULT="$(cd $BIN_DIR/../var/captain/ships;pwd)"
SHIP_STATUS_FILE="$CONF_DIR/ship_status"
SHIP_NAME=""
SHIP_INSTALL_PATH=""
SHIP_HULL_BASE=""
SHIP_INSTALL_TYPE=""
SHIP_NETBSD_VERSION=""
SHIP_IP=""
SHIP_STATUS=""
SHIP_NET=""
NETBSD_INSTALL_TYPE=0
HOST_ETH_LIST=""
MINIMAL_SETS_ARRAY="base etc kern-GENERIC"
FULL_SETS_ARRAY="base etc kern-GENERIC comp debug games man misc modules rescue tests text"
FULL_WITH_X_SETS_ARRAY="base etc kern-GENERIC comp debug games man misc modules rescue tests text xbase xcomp xdebug xetc xfont xserver"
VALID_NETBSD_VERSIONS="7.1 7.1.1 7.1.2 7.2 8.0 8.1 8.2 9.0 9.1 9.2"
GZ_SETS_NETBSD_VERSIONS="7.1 7.1.1 7.1.2 7.2 8.0 8.1 8.2"
COMPRESS_FORMAT=""
VERBOSE_LEVEL=2

# Overwrite defaults by importing captain.conf
. $CONF_DIR/captain.conf

usage()
{
	verbose "SHIP" 1
	verbose "usage: $0 ship start <ship_name>" 1
	verbose "usage: $0 ship stop <ship_name>" 1
	verbose "usage: $0 ship build <ship_name>" 1
	verbose "usage: $0 ship delete <ship_name>" 1
	verbose "usage: $0 ship destroy <ship_name>" 1
	verbose "usage: $0 ship blueprint <ship_name>" 1
	verbose "usage: $0 ship ls|list" 1
	verbose "" 1 
	verbose "HULL" 1
	verbose "usage: $0 hull blueprint <hull_name>" 1
	verbose "usage: $0 hull build <hull_name>" 1
	verbose "usage: $0 hull destroy <hull_name>" 1
	verbose "usage: $0 hull delete <blueprint_name>" 1
	verbose "usage: $0 hull download-sets" 1
	verbose "usage: $0 hull flush-sets" 1
	verbose "usage: $0 hull ls|list" 1
	verbose "" 1
	exit 1
}
hull_config_wizard()
{
	while [ "$HULL_NAME" == "" ] || [ "$(validate_name_not_taken $HULL_NAME $HULL_STATUS_FILE)" -eq 1 ]
	do
		echo -n "Enter hull name followed by [ENTER]: "
		read HULL_NAME
		if [ "$HULL_NAME" == "" ]
		then
			echo "Error: Hull name cannot be empty"
		elif [ "$(validate_name_not_taken $HULL_NAME $HULL_STATUS_FILE)" -eq 1 ]
		then
			echo "Error: Hull name already taken. Please choose another name"
		fi
	done
	while [ ! -d "$HULL_INSTALL_PATH" ]
	do
		echo -n "Enter hull path followed by [ENTER] (default $HULL_INSTALL_PATH_DEFAULT/$HULL_NAME): "
		read HULL_INSTALL_PATH
		if [ "$HULL_INSTALL_PATH" == "" ]
		then
			HULL_INSTALL_PATH=$HULL_INSTALL_PATH_DEFAULT/$HULL_NAME
		fi
		validate_path_exist $HULL_INSTALL_PATH $HULL_NAME
		validate_path_not_taken $HULL_INSTALL_PATH $HULL_NAME $HULL_STATUS_FILE
	done
	while [ "$NETBSD_VERSION" == "" ] || [ "$(validate_netbsd_version $NETBSD_VERSION)" -eq 1 ]
	do
		echo "Plese select NetBSD version"
		echo "Available versions are : "
		for VERSION in $VALID_NETBSD_VERSIONS
		do
			echo $VERSION
		done
		echo -n "Enter NetBSD version followed by [ENTER]: "
		read NETBSD_VERSION 
		if [ "$NETBSD_VERSION" == "" ]
		then
			echo "Error: NetBSD version cannot be empty"
		elif [ "$(validate_netbsd_version $NETBSD_VERSION)" -eq 1 ]
		then
			echo "Error: Invalid NetBSD version"
		fi
	done
	while [ "$NETBSD_INSTALL_TYPE" -ne "1" -a "$NETBSD_INSTALL_TYPE" -ne "2" -a "$NETBSD_INSTALL_TYPE" -ne "3" ]
	do
		echo "Enter NetBSD Installation type"
		echo "Choices are :"
		echo "1- Minimal"
		echo "2- Full Without X"
		echo "3- Full With X"
		echo -n "Enter NetBSD Installation type followed by [ENTER]: "
		read NETBSD_INSTALL_TYPE 
		if [ "$NETBSD_INSTALL_TYPE" == "" ]
		then
			echo "Error: NetBSD Install type cannot be empty" 
		fi
		if [ "$NETBSD_INSTALL_TYPE" -ne "1" -a "$NETBSD_INSTALL_TYPE" -ne "2" -a "$NETBSD_INSTALL_TYPE" -ne "3" ]
		then	
			echo "Error: Invalid choice" 
			read NETBSD_INSTALL_TYPE
		fi
	done
	blueprint_hull $HULL_NAME $HULL_INSTALL_PATH $NETBSD_VERSION $NETBSD_INSTALL_TYPE
}
ship_config_wizard()
{
	while [ "$SHIP_NAME" == "" ] || [ "$(validate_name_not_taken $SHIP_NAME $SHIP_STATUS_FILE)" -eq 1 ]
	do
		echo -n "Enter ship name followed by [ENTER]: "
		read SHIP_NAME
		if [ "$SHIP_NAME" == "" ]
		then
			echo "Error: Ship name cannot be empty"
		elif [ "$(validate_name_not_taken $SHIP_NAME $SHIP_STATUS_FILE)" -eq 1 ]
		then
			echo "Error: Ship name already taken. Please choose another ship name"
		fi
	done
        while [ ! -d "$SHIP_PATH" ]
        do
                echo -n "Enter ship path followed by [ENTER] (default $SHIP_PATH_DEFAULT/$SHIP_NAME): "
                read SHIP_PATH
                if [ "$SHIP_PATH" == "" ]
                then
                        SHIP_PATH=$SHIP_PATH_DEFAULT/$SHIP_NAME
                fi
                validate_path_exist $SHIP_PATH $HULL_NAME
                validate_path_not_taken $SHIP_PATH $SHIP_NAME $SHIP_STATUS_FILE
        done
	get_hull_names_list
	while [ "$SHIP_HULL_BASE" == "" ]
	do
		echo "From which hull do you want your ship built?"
		echo "Available Hulls : "		
		list_hull
		echo -n "Enter hull name followed by [ENTER] "		
		read SHIP_HULL_BASE
		while [ "$(validate_hull_exist $SHIP_HULL_BASE $HULL_STATUS_FILE)" -eq 0 ]
		do
			echo -n "Invalid Hull Name "
			echo "From which hull do you want your ship built?"
			echo "Available Hulls : "
			list_hull
			echo -n "Enter hull name followed by [ENTER] "
			read SHIP_HULL_BASE
		done
	done
	get_hull_install_type $SHIP_HULL_BASE $HULL_STATUS_FILE
	SHIP_INSTALL_TYPE=$HULL_INSTALL_TYPE
	get_hull_netbsd_version $SHIP_HULL_BASE $HULL_STATUS_FILE
	SHIP_NETBSD_VERSION=$HULL_NETBSD_VERSION
	while [ "$SHIP_IP" == "" ] 
	do
		echo "What IP should the ship have?"
		echo -n "Enter IP followed by [ENTER]: "
		read SHIP_IP
		validate_ip_format $SHIP_IP
		if [ $? -eq 1 ]
		then
			echo "Invalid IP"
			echo $SHIP_IP
			SHIP_IP=""
		else
			echo $SHIP_IP
		fi
	done
	get_host_eth_list
	VALID_ETH=0
	while [ "$SHIP_NET" == "" ] || [ $VALID_ETH -ne 1 ]
	do
		echo "What net interface should the ship use?"
		echo "Here is the list of interfaces :" 
		echo "$HOST_ETH_LIST"
		echo -n "Enter interface followed by [ENTER]: "
		read SHIP_NET
		for ETH in $HOST_ETH_LIST
		do
			if [ "$ETH" == "$SHIP_NET" ]
			then
				VALID_ETH=1
			fi
		done
		if [ $VALID_ETH -ne 1 ]
		then
			echo "Invalid network interface"
		fi
	done
	blueprint_ship $SHIP_NAME $SHIP_PATH $SHIP_HULL_BASE $SHIP_NETBSD_VERSION $SHIP_INSTALL_TYPE $SHIP_IP $SHIP_NET
}
list_hull()
{
	exit_if_file_not_found $HULL_STATUS_FILE
	local LINES="--------------------------------------------------------------------------------------------------"
	echo $LINES
	printf "|%-16s %-40s %-8s %-16s %-12s|\n" NAME PATH VERS INSTALL_TYPE STATUS
	echo $LINES
	for line in $(cat $HULL_STATUS_FILE)
	do
		HULL_NAME="$(echo $line|cut -d ',' -f 1)"
		HULL_INSTALL_PATH="$(echo $line|cut -d ',' -f 2)"
		HULL_NETBSD_VERSION="$(echo $line|cut -d ',' -f 3)"
		NETBSD_INSTALL_TYPE="$(echo $line|cut -d ',' -f 4)"
		HULL_STATUS="$(echo $line|cut -d ',' -f 5)"
		local TRIMMED_HULL_NAME=$(echo $HULL_NAME | cut -c -16 )
		local TRIMMED_HULL_INSTALL_PATH=$(echo $HULL_INSTALL_PATH | cut -c -40 )
		local VERBOSE_NETBSD_INSTALL_TYPE=""
		if [ $NETBSD_INSTALL_TYPE -eq 1 ]
		then
			VERBOSE_NETBSD_INSTALL_TYPE="Minimal"
		elif [ $NETBSD_INSTALL_TYPE -eq 2 ]
		then
			VERBOSE_NETBSD_INSTALL_TYPE="FullNoX"
		elif [ $NETBSD_INSTALL_TYPE -eq 3 ]
		then
			VERBOSE_NETBSD_INSTALL_TYPE="FullWithX"
		fi

		printf "|%-16s %-40s %-8s %-16s %-12s|\n" $TRIMMED_HULL_NAME $TRIMMED_HULL_INSTALL_PATH $HULL_NETBSD_VERSION $VERBOSE_NETBSD_INSTALL_TYPE $HULL_STATUS
	done
	echo $LINES
	
}
long_list_ship()
{
	exit_if_file_not_found $SHIP_STATUS_FILE
	local LINES="-----------------------------------------------------------------------------------------------------------------------------------------------"
	echo $LINES
	printf "|%-16s %-42s %-16s %-8s %-16s %-8s %-16s %-12s|\n" NAME PATH HULL VERS INSTALL_TYPE NET IP STATUS
	echo $LINES
	for line in $(cat $SHIP_STATUS_FILE)
	do
		SHIP_NAME="$(echo $line|cut -d ',' -f 1)"
		get_ship_install_path $SHIP_NAME
		get_ship_hull_base $SHIP_NAME
		get_ship_netbsd_version $SHIP_NAME
		get_ship_install_type $SHIP_NAME
		get_ship_ip $SHIP_NAME
		get_ship_net $SHIP_NAME
		get_ship_status $SHIP_NAME
		local TRIMMED_SHIP_NAME=$(echo $SHIP_NAME | cut -c -16 )
		local TRIMMED_SHIP_PATH=$(echo $SHIP_INSTALL_PATH | cut -c -42 )
		local VERBOSE_NETBSD_INSTALL_TYPE=""
		if [ $SHIP_INSTALL_TYPE -eq 1 ]
		then
			VERBOSE_NETBSD_INSTALL_TYPE="Minimal"
		elif [ $SHIP_INSTALL_TYPE -eq 2 ]
		then
			VERBOSE_NETBSD_INSTALL_TYPE="FullNoX"
		elif [ $SHIP_INSTALL_TYPE -eq 3 ]
		then
			VERBOSE_NETBSD_INSTALL_TYPE="FullWithX"
		fi

		printf "|%-16s %-42s %-16s %-8s %-16s %-8s %-16s %-12s|\n" $TRIMMED_SHIP_NAME $TRIMMED_SHIP_PATH $SHIP_HULL_BASE $SHIP_NETBSD_VERSION $VERBOSE_NETBSD_INSTALL_TYPE $SHIP_NET $SHIP_IP $SHIP_STATUS
	done
	echo $LINES
}
list_ship()
{
	exit_if_file_not_found $SHIP_STATUS_FILE
	local LINES="---------------------------------------------------------"
	echo $LINES
	printf "|%-16s %-8s %-16s %-12s|\n" NAME NET IP STATUS
	echo $LINES
	for line in $(cat $SHIP_STATUS_FILE)
	do
		SHIP_NAME="$(echo $line|cut -d ',' -f 1)"
		get_ship_ip $SHIP_NAME
		get_ship_net $SHIP_NAME
		get_ship_status $SHIP_NAME
		local TRIMMED_SHIP_NAME=$(echo $SHIP_NAME | cut -c -16 )
		printf "|%-16s %-8s %-16s %-12s|\n" $TRIMMED_SHIP_NAME $SHIP_NET $SHIP_IP $SHIP_STATUS
	done
	echo $LINES
}
blueprint_hull()
{
	local HULL_NAME=$1
	local HULL_INSTALL_PATH=$2
	local NETBSD_VERSION=$3
	local NETBSD_INSTALL_TYPE=$4
	local STATUS="configured"
	echo "$HULL_NAME,$HULL_INSTALL_PATH,$NETBSD_VERSION,$NETBSD_INSTALL_TYPE,$STATUS" >> $HULL_STATUS_FILE
}
blueprint_ship()
{
	local SHIP_NAME=$1
	local SHIP_PATH=$2
	local BASE_HULL=$3
	local NETBSD_VERSION=$4
	local NETBSD_INSTALL_TYPE=$5
	local SHIP_IP=$6
	local SHIP_NET=$7
	local STATUS="configured"
	echo "$SHIP_NAME,$SHIP_PATH,$BASE_HULL,$NETBSD_VERSION,$NETBSD_INSTALL_TYPE,$SHIP_IP,$SHIP_NET,$STATUS" >> $SHIP_STATUS_FILE
}
build_hull()
{
	local HULL_NAME=$1
        local HULL_INSTALL_PATH=""
        local NETBSD_VERSION=""
        local NETBSD_INSTALL_TYPE=""
	local HULL_STATUS=""
	local OBJECT_TYPE="hull"
	get_hull_status $HULL_NAME
	if [ ! "$HULL_STATUS" == "configured" ]
	then
		verbose "Error : Hull $HULL_NAME is not in configured mode" 1	
		exit 1
	fi
	get_hull_install_type $HULL_NAME	
	get_hull_install_path $HULL_NAME
	validate_path_exist $HULL_INSTALL_PATH
	get_hull_netbsd_version $HULL_NAME
	get_compress_format $HULL_NETBSD_VERSION
	if [ "$HULL_INSTALL_TYPE" -eq 1 ]
	then
		local SET_ARRAY="$MINIMAL_SETS_ARRAY"
	elif [ "$HULL_INSTALL_TYPE" -eq 2 ]
	then
		local SET_ARRAY="$FULL_SETS_ARRAY"
	elif [ "$HULL_INSTALL_TYPE" -eq 3 ]
	then
		local SET_ARRAY="$FULL_WITH_X_SETS_ARRAY"
	else
		verbose "Error : Unknown install type" 1
		verbose "Error : You shouldn't be here. There is only 3 choices : FullWithX,FullNoX or minimal" 2
		exit 1
	fi
	for SET in $SET_ARRAY
	do
		#Make sure all sets in the array are downloaded, if not present call download_tarball function
		if ! [ -f $CACHE_DIR/NetBSD-$HULL_NETBSD_VERSION/$ARCHITECTURE/$SET.$COMPRESS_FORMAT ]
		then
			verbose "$SET not found" 1 
			download_tarball $SET.$COMPRESS_FORMAT $HULL_NETBSD_VERSION
		fi
		verbose "Installing $SET" 1
		tar -xf $CACHE_DIR/NetBSD-$HULL_NETBSD_VERSION/$ARCHITECTURE/$SET.$COMPRESS_FORMAT -C $HULL_INSTALL_PATH
	done	
	set_hull_status $HULL_NAME "installed"
}
build_ship()
{
	local SHIP_NAME=$1
	get_ship_status $SHIP_NAME	
	if [ "$SHIP_STATUS" == "configured" ]
	then
		verbose "Building ship $SHIP_NAME" 1
	else
		verbose "Error : Ship $SHIP_NAME is not in configured mode" 1
		exit 1
	fi
	get_ship_hull_base $SHIP_NAME
	local HULL_NAME="$SHIP_HULL_BASE"
	get_hull_status $HULL_NAME	
	if [ "$HULL_STATUS" != "installed" ]
	then
		verbose "Error : Hull is not in installed mode" 1
		exit 1
	fi
	get_ship_install_path $SHIP_NAME
	validate_path_exist $SHIP_INSTALL_PATH
	get_hull_install_path $HULL_NAME
	verbose "Installing ship $SHIP_NAME" 2
	cp -a $HULL_INSTALL_PATH/* $SHIP_INSTALL_PATH
	cd $SHIP_INSTALL_PATH/dev/
	verbose "Creating DEV" 2
	./MAKEDEV all
	verbose "Ajusting permissions" 2
	chmod 1777 $SHIP_INSTALL_PATH/var/tmp
	verbose  "Copying .profile" 2
	echo "export PS1=\"$SHIP_NAME # \"" >>  $SHIP_INSTALL_PATH/root/.profile
	echo "export PS1=\"$SHIP_NAME # \"" >>  $SHIP_INSTALL_PATH/etc/skel/.profile
	ask_user_yes_no 'Would you like to copy host /etc/hosts in ship ? (Y/y or N/n) '
	if [[ "$ANSWER" == "y"  ||  "$ANSWER" == "Y" ]]	
	then
		exit_if_file_not_found /etc/hosts
		verbose "Copying hosts file" 2
		cp /etc/hosts $SHIP_INSTALL_PATH/etc/hosts
	fi
	ask_user_yes_no 'Would you like to run a post-install script ? (Y/y or N/n) '
	if [ "$ANSWER" == "y" ] || [ "$ANSWER" == "Y" ]
	then
		echo "Please enter the full path and script name"	
		read POST_INSTALL_SCRIPT
		while ! $(validate_file_presence $POST_INSTALL_SCRIPT)
		do	
			echo "File $POST_INSTALL_SCRIPT not found"
			echo "Please enter the full path and script name"
			read POST_INSTALL_SCRIPT
		done
		verbose "File $POST_INSTALL_SCRIPT found. Continuing" 2
		POST_INSTALL_SCRIPT_NAME=$(basename $POST_INSTALL_SCRIPT)
		# If the file is there copy it inside root home directory inside ship
		cp $POST_INSTALL_SCRIPT $SHIP_INSTALL_PATH/root/
		# chroot inside ship and execute the script
		chroot $SHIP_INSTALL_PATH /root/$POST_INSTALL_SCRIPT_NAME
		# Hope for the best
	fi
	set_ship_status $SHIP_NAME "installed"
	verbose "Ship $SHIP_NAME installed" 1
}
get_hull_status()
{
	#This function will update global variable HULL_STATUS
	local HULL_NAME=$1
	local OBJECT_TYPE="hull"
	exit_if_file_not_found $HULL_STATUS_FILE
	validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	HULL_STATUS=$(grep $HULL_NAME $HULL_STATUS_FILE | cut -d ',' -f 5)
}
set_hull_status()
{
        local HULL_NAME=$1
	local TARGET_STATUS=$2
        local OBJECT_TYPE="hull"
        exit_if_file_not_found $HULL_STATUS_FILE
        validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	get_hull_install_path $HULL_NAME
	get_hull_install_type $HULL_NAME
	get_hull_netbsd_version $HULL_NAME
	SOURCE_STRING="$HULL_NAME,$HULL_INSTALL_PATH,$HULL_NETBSD_VERSION,$HULL_INSTALL_TYPE,$HULL_STATUS"
	TARGET_STRING="$HULL_NAME,$HULL_INSTALL_PATH,$HULL_NETBSD_VERSION,$HULL_INSTALL_TYPE,$TARGET_STATUS"
	sed -i 's|'"$SOURCE_STRING"'|'"$TARGET_STRING"'|g' $HULL_STATUS_FILE 		
}
get_hull_install_type()
{
	local HULL_NAME=$1
	local OBJECT_TYPE="hull"
	exit_if_file_not_found $HULL_STATUS_FILE
	validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	HULL_INSTALL_TYPE=$(grep $HULL_NAME $HULL_STATUS_FILE | cut -d ',' -f 4)
}
get_hull_install_path()
{
	local HULL_NAME=$1
	local OBJECT_TYPE="hull"
	exit_if_file_not_found $HULL_STATUS_FILE
	validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	HULL_INSTALL_PATH=$(grep $HULL_NAME $HULL_STATUS_FILE | cut -d ',' -f 2)
}
get_hull_netbsd_version()
{
	local HULL_NAME=$1
	local OBJECT_TYPE="hull"
	exit_if_file_not_found $HULL_STATUS_FILE
	validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	HULL_NETBSD_VERSION=$(grep $HULL_NAME $HULL_STATUS_FILE | cut -d ',' -f 3)
}
get_ship_install_path()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_INSTALL_PATH=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 2)
}
get_ship_hull_base()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_HULL_BASE=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 3)
}
get_ship_netbsd_version()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_NETBSD_VERSION=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 4)
}
get_ship_install_type()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_INSTALL_TYPE=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 5)
}
get_ship_ip()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_IP=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 6)
}
get_ship_net()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_NET=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 7)
}
get_ship_status()
{
	#This function will update global variable SHIP_STATUS
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	SHIP_STATUS=$(grep $SHIP_NAME $SHIP_STATUS_FILE | cut -d ',' -f 8)
}
set_ship_status()
{
        local SHIP_NAME=$1
	local TARGET_STATUS=$2
        local OBJECT_TYPE="ship"
        exit_if_file_not_found $SHIP_STATUS_FILE
        validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	get_ship_install_path $SHIP_NAME
	get_ship_hull_base $SHIP_NAME
	get_ship_install_type $SHIP_NAME
	get_ship_netbsd_version $SHIP_NAME
	get_ship_ip $SHIP_NAME
	get_ship_net $SHIP_NAME
	get_ship_status $SHIP_NAME
	SOURCE_STRING="$SHIP_NAME,$SHIP_INSTALL_PATH,$SHIP_HULL_BASE,$SHIP_NETBSD_VERSION,$SHIP_INSTALL_TYPE,$SHIP_IP,$SHIP_NET,$SHIP_STATUS"
	verbose "SOURCE_STRING $SOURCE_STRING" 9
	TARGET_STRING="$SHIP_NAME,$SHIP_INSTALL_PATH,$SHIP_HULL_BASE,$SHIP_NETBSD_VERSION,$SHIP_INSTALL_TYPE,$SHIP_IP,$SHIP_NET,$TARGET_STATUS"
	verbose "TARGET_STRING $TARGET_STRING" 9
	sed -i 's|'"$SOURCE_STRING"'|'"$TARGET_STRING"'|g' $SHIP_STATUS_FILE 		
}
get_compress_format()
{
	local NETBSD_VERSION=$1
	# tar.xz is used for NetBSD 9.0 onward on amd64 only. If the version id older and of it's not amd64 it is a tgz
	COMPRESS_FORMAT="tar.xz"
	if [ "$ARCHITECTURE" != "amd64" ]
	then
		COMPRESS_FORMAT="tgz"
	else
		for GZ_VERSION in $GZ_SETS_NETBSD_VERSIONS
		do
			if [ "$GZ_VERSION" == "$NETBSD_VERSION" ]
			then
				COMPRESS_FORMAT="tgz"
			fi
		done
	fi
}
get_hull_names_list()
{
	exit_if_file_not_found $HULL_STATUS_FILE
	for line in $(cat $HULL_STATUS_FILE)
	do
		HULL_NAMES_LIST="$HULL_NAMES_LIST $(echo $line|cut -d ',' -f 1)"
	done
}
get_host_eth_list()
{
	#Until I find a better way to get the host eth interfaces this will do
	HOST_ETH_LIST="$(ifconfig -a | grep mtu | cut -d : -f1)"
}
delete_hull()
{
	local HULL_NAME=$1
	local OBJECT_TYPE="hull"
	exit_if_file_not_found $HULL_STATUS_FILE
	validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	get_hull_install_path $HULL_NAME
	get_hull_netbsd_version $HULL_NAME
	get_hull_install_type $HULL_NAME
	get_hull_status $HULL_NAME
	if [ "$HULL_STATUS" != "configured" ]
	then
		echo "Error : $HULL_NAME is not in configured mode"
		exit 1
	fi
	ask_user_yes_no "Are you sure you want to delete that hull : $HULL_NAME ?"
	if [ $ANSWER == "y" ] || [ $ANSWER == "Y" ]
	then
		STRING_TO_REMOVE="$HULL_NAME,$HULL_INSTALL_PATH,$HULL_NETBSD_VERSION,$HULL_INSTALL_TYPE,configured"
		LINE_TO_REMOVE="$(grep -nw $STRING_TO_REMOVE $HULL_STATUS_FILE |cut -d ':' -f 1)"
		sed -i "$LINE_TO_REMOVE"'d' $HULL_STATUS_FILE
		verbose "Deleted hull $HULL_NAME config" 1
	else 
		verbose "Hull $HULL_NAME not deleted" 1
	fi
}
delete_ship()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	exit_if_file_not_found $SHIP_STATUS_FILE
	validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
	get_ship_install_path $SHIP_NAME
	get_ship_hull_base $SHIP_NAME
	get_ship_netbsd_version $SHIP_NAME
	get_ship_install_type $SHIP_NAME
	get_ship_ip $SHIP_NAME
	get_ship_net $SHIP_NAME
	get_ship_status $SHIP_NAME
	if [ "$SHIP_STATUS" != "configured" ]
	then
		verbose "Error : $SHIP_NAME is not in configured mode" 1
		verbose "Error : if you want to delete a ship configuration do not forget to destroy(uninstall) it first" 1
		exit 1
	fi
	ask_user_yes_no "Are you sure you want to delete that ship : $SHIP_NAME ?"
	if [ $ANSWER == "y" ] || [ $ANSWER == "Y" ]
	then
		STRING_TO_REMOVE="$SHIP_NAME,$SHIP_INSTALL_PATH,$SHIP_HULL_BASE,$SHIP_NETBSD_VERSION,$SHIP_INSTALL_TYPE,$SHIP_IP,$SHIP_NET,configured"
		LINE_TO_REMOVE="$(grep -nw $STRING_TO_REMOVE  $SHIP_STATUS_FILE|cut -d ':' -f 1)"
		sed -i "$LINE_TO_REMOVE"'d' $SHIP_STATUS_FILE
		verbose "Deleted ship $SHIP_NAME config" 1
	else
		verbose "Ship $SHIP_NAME not deleted" 1
	fi
}
destroy_hull()
{
	local HULL_NAME=$1
	local OBJECT_TYPE="hull"
	verbose "Destroying/Uninstalling hull $HULL_NAME" 1
	exit_if_file_not_found $HULL_STATUS_FILE
	validate_object_presence_in_state_file $HULL_NAME $HULL_STATUS_FILE $OBJECT_TYPE
	get_hull_status $HULL_NAME
	get_hull_install_path $HULL_NAME
	if [ "$HULL_STATUS" != "installed" ]
	then
		verbose "Error : $HULL_NAME is not in installed mode" 1
		exit 1
	fi
	ask_user_yes_no "Are you sure you want to destroy that hull : $HULL_NAME ?"
	if [ $ANSWER == "y" ] || [ $ANSWER == "Y" ]
	then
		rm -fr $HULL_INSTALL_PATH
		set_hull_status $HULL_NAME "configured"	
		verbose "Hull $HULL_NAME destroyed" 1
		verbose "You might want to run \"captain hull destroy $HULL_NAME to remove any configuration\"" 2
	else
		verbose "Hull $HULL_NAME not destroyed" 1
	fi
}
destroy_ship()
{
	local SHIP_NAME=$1
	local OBJECT_TYPE="ship"
	ask_user_yes_no "Are you sure you want to destroy/uninstall the ship $SHIP_NAME ? (Y/y or N/n)"
        if [[ "$ANSWER" == "y"  ||  "$ANSWER" == "Y" ]]
	then
		verbose "Destroying/Uninstalling $OBJECT_TYPE $SHIP_NAME" 1
		exit_if_file_not_found $SHIP_STATUS_FILE
		validate_object_presence_in_state_file $SHIP_NAME $SHIP_STATUS_FILE $OBJECT_TYPE
		get_ship_status $SHIP_NAME
		get_ship_install_path $SHIP_NAME
		if [ "$SHIP_STATUS" != "installed" ] && [ "$SHIP_STATUS" != "configured" ]
		then
			verbose "Error : $SHIP_NAME is not in installed or configured mode" 1
			exit 1
		fi
		if [ "$SHIP_STATUS" == "installed" ]
		then
			rm -fr $SHIP_INSTALL_PATH
			set_ship_status $SHIP_NAME "configured"	
			verbose "Switching ship $SHIP_NAME to configured mode" 2 
			verbose "You will have to run captain ship delete to remove configuration" 3
	
		fi
		if [ "$SHIP_STATUS" == "configured" ]
		then
			rm -fr $SHIP_INSTALL_PATH
			verbose "Ship $SHIP_NAME was already in configured mode. Removed anything in $SHIP_INSTALL_PATH just to be sure" 2
			verbose "You will have to run captain ship delete to remove configuration" 3
		fi
	else
		verbose "Ship $SHIP_NAME not destroyed" 1
		exit 0
	fi
}
board_ship()
{
	local SHIP_NAME=$1
	get_ship_status $SHIP_NAME
	get_ship_install_path $SHIP_NAME
	get_ship_ip $SHIP_NAME
	# Validate Ship is running or installed
	if [ "$SHIP_STATUS" == "installed" ] || [ "$SHIP_STATUS" == "running" ]
	then
		verbose "Chrooting in $SHIP_NAME" 1
		verbose "This ship has this ip : $SHIP_IP" 1
		verbose "Remember : NetBSD has no network isolation. It is your responsibility to make sure your services listen to this IP" 2
		chroot $SHIP_INSTALL_PATH  /usr/bin/env -i PS1="$SHIP_NAME # " /bin/ksh
	else
		verbose "Ship $SHIP_NAME is not installed" 1
	fi
}
reset_running_ship_status()
{
	ask_user_yes_no "Are you sure you want to reset all ship status from running to installed ? (Usually mean there has been a power outage or the host crashed) "
	verbose "ANSWER $ANSWER" 9
	if [ "$ANSWER" == "Y" ] || [ "$ANSWER" == "y" ] 	
	then
	        for line in $(cat $SHIP_STATUS_FILE)
        	do
               	 	SHIP_NAME="$(echo $line|cut -d ',' -f 1)"
			get_ship_status $SHIP_NAME
			if [ $SHIP_STATUS == "running" ]
			then
				verbose "Changing $SHIP_NAME status to installed" 2
				set_ship_status $SHIP_NAME "installed"
			fi
		done
	else
		verbose "Running ship reset status cancelled" 2	
	fi


}
start_ship()
{
	local SHIP_NAME=$1
	get_ship_status $SHIP_NAME
	get_ship_install_path $SHIP_NAME
	get_ship_ip $SHIP_NAME
	get_ship_net $SHIP_NAME
	if [ "$SHIP_STATUS" != "installed" ]
	then
		verbose "Ship $SHIP_NAME is not installed. Make sure the ship is installed and not running" 1
		exit 1
	fi
	validate_net_interface $SHIP_NET
	add_ip_on_net $SHIP_IP $SHIP_NET
	#bind moount pts and other specials	
	mount_specials $SHIP_NAME
	rcd_action $SHIP_NAME "start"
	set_ship_status $SHIP_NAME "running"
}
stop_ship()
{
	local SHIP_NAME=$1
	get_ship_status $SHIP_NAME
	get_ship_install_path $SHIP_NAME
	get_ship_ip $SHIP_NAME
	get_ship_net $SHIP_NAME
	if [ "$SHIP_STATUS" != "running" ]
	then
		verbose "Ship $SHIP_NAME is not running. Make sure the ship is running if you wish to stop it" 1
		exit 1
	fi
	rcd_action $SHIP_NAME "stop"
	validate_net_interface $SHIP_NET
	remove_ip_on_net $SHIP_IP $SHIP_NET
	umount_specials $SHIP_NAME
	set_ship_status $SHIP_NAME "installed"
}
radio_ship()
{
	SHIP_NAME=$1
	COMMAND="$2"
	get_ship_status $SHIP_NAME
	if [ "$SHIP_STATUS" != "running" ]
	then
		verbose "Ship $SHIP_NAME is not running. Make sure the ship is running if you wish to stop it" 1
		exit 1
	fi
	get_ship_install_path $SHIP_NAME
	chroot $SHIP_INSTALL_PATH $COMMAND
}
strip_ship()
{
	# Some TODO here. Look at the end of the function
	SHIP_NAME=$1
	get_ship_status $SHIP_NAME
	get_ship_install_path $SHIP_NAME
	get_ship_netbsd_version $SHIP_NAME
	get_ship_install_type $SHIP_NAME
	if [ "$SHIP_STATUS" != "installed" ]
	then
		echo "Ship $SHIP_NAME is not in installed state. Make sure the ship is install but not running"
		exit 1
	fi
	# Offer to change ship name
	local ANSWER=""
	while [ "$ANSWER" != "Y" ] && [ "$ANSWER" != "y" ] && [ "$ANSWER" != "N" ] && [ "$ANSWER" != "n" ]
	do
		verbose "Would you like to change the name of the futur hull ? (Actual name is $SHIP_NAME)" 1	
		read ANSWER
		if  [ $ANSWER != "Y" ] && [ $ANSWER != "y" ] && [ $ANSWER != "N" ] && [ $ANSWER != "n" ]
		then
			verbose "Invalid choice" 1
		fi
		
	done
	if [ "$ANSWER" == "Y" ] || [ "$ANSWER" == "y" ]
	then
		echo "Please entre hull name"
		read HULL_NAME
		while [ $HULL_NAME == "" ] || [ "$(validate_name_not_taken $HULL_NAME $HULL_STATUS_FILE)" -eq 1 ]
		do
			if [ $HULL_NAME == "" ] 
			then
				verbose "Error: hull name cannot be empty" 1
			elif [ "$(validate_name_not_taken $HULL_NAME $HULL_STATUS_FILE)" -eq 1 ]
			then
				verbose "Error: hull name already taken. Please choose another" 1
			fi
			read HULL_NAME
		done
	else
		HULL_NAME=$SHIP_NAME
	fi
	# Validate if we are using default path or not
	while [ ! -d "$HULL_INSTALL_PATH" ]
        do
                echo -n "Enter hull path followed by [ENTER] (default $HULL_INSTALL_PATH_DEFAULT/$HULL_NAME): "
                read HULL_INSTALL_PATH
                if [ "$HULL_INSTALL_PATH" == "" ]
                then
                        HULL_INSTALL_PATH=$HULL_INSTALL_PATH_DEFAULT/$HULL_NAME
                fi
                validate_path_exist $HULL_INSTALL_PATH $HULL_NAME
                validate_path_not_taken $HULL_INSTALL_PATH $HULL_NAME $HULL_STATUS_FILE
        done
	HULL_INSTALL_PATH=$HULL_INSTALL_PATH_DEFAULT/$HULL_NAME	
	validate_path_exist $HULL_INSTALL_PATH
	# Validate empty directory
	if [ "($(ls $HULL_INSTALL_PATH | wc -l | awk '{print $1}')" -ne 0 ]
	then
		verbose "Path $HULL_INSTALL_PATH Does not seem to be empty" 1
		exit 1
	fi
	# Copy Ship to Hull PATH
	cp -rp $SHIP_INSTALL_PATH $HULL_INSTALL_PATH
	# Convert ship to hull 
	# Add Hull to hull_status_file
	HULL_NETBSD_VERSION=$SHIP_NETBSD_VERSION
	HULL_INSTALL_TYPE=$SHIP_INSTALL_TYPE
	blueprint_hull $HULL_NAME $HULL_INSTALL_PATH $NETBSD_VERSION $NETBSD_INSTALL_TYPE
	# destroy_ship $SHIP_NAME
	# delete_ship $SHIP_NAME
}
mount_specials()
{
	verbose "Mounting specials" 2
	local SHIP_NAME=$1
	get_ship_install_path $SHIP_NAME
	#Up to this point the ship should be created. If it's not, there is something really wrong
	#Might be a broken ard drive or mount point 
	fail_if_path_not_exist $SHIP_INSTALL_PATH/dev/pts
	mount_if_not_mounted "ptyfs" "ptyfs" "$SHIP_INSTALL_PATH/dev/pts/"
}
umount_specials()
{
	verbose "Umounting specials" 2
	local SHIP_NAME=$1
	get_ship_install_path $SHIP_NAME
	fail_if_path_not_exist $SHIP_INSTALL_PATH/dev/pts
	umount_if_mounted "$SHIP_INSTALL_PATH/dev/pts"
}
add_ip_on_net()
{
	local IP_TO_VALIDATE=$1
	local NET_TO_VALIDATE=$2
	ifconfig $NET_TO_VALIDATE | grep $IP_TO_VALIDATE 2>1 >> /dev/null
	if [ $? -eq 1 ]
	then
		#IP $IP_TO_VALIDATE does not seems to be on net $NET_TO_VALIDATE
		verbose "Addind $IP_TO_VALIDATE to $NET_TO_VALIDATE" 1
		ifconfig $NET_TO_VALIDATE $IP_TO_VALIDATE/24 alias
	else
		verbose "IP $IP_TO_VALIDATE is already up on $NET_TO_VALIDATE" 1
	fi

}
remove_ip_on_net()
{
	local IP_TO_VALIDATE=$1
	local NET_TO_VALIDATE=$2
	ifconfig $NET_TO_VALIDATE | grep $IP_TO_VALIDATE 2>1 >> /dev/null
	if [ $? -eq 0 ]
	then
		#IP $IP_TO_VALIDATE seems to be on net $NET_TO_VALIDATE
		verbose "Removing $IP_TO_VALIDATE to $NET_TO_VALIDATE" 1
		ifconfig $NET_TO_VALIDATE $IP_TO_VALIDATE/24 -alias
	else
		verbose "IP $IP_TO_VALIDATE is already removed from $NET_TO_VALIDATE" 1
	fi
}
mount_if_not_mounted()
{
	local MOUNT_TYPE=$1
	local MOUNT_OBJECT=$2
	local PATH_TO_MOUNT=$3
	if mount | grep $PATH_TO_MOUNT
	then
		verbose "$PATH_TO_MOUNT is already mounted" 2
	else
		verbose "mounting $PATH_TO_MOUNT" 2
		mount -t $MOUNT_TYPE $MOUNT_OBJECT $PATH_TO_MOUNT
	fi
	
}
umount_if_mounted()
{
	PATH_TO_UMOUNT=$1
	if mount | grep $PATH_TO_UMOUNT
	then
		verbose "Umounting $PATH_TO_UMOUNT" 2
		umount $PATH_TO_UMOUNT
	else
		verbose "$PATH_TO_UMOUNT already umounted" 2
	fi
	
}
rcd_action()
{
	local SHIP_NAME=$1
	local ACTION=$2
	get_ship_install_path $SHIP_NAME
	if [ -f $SHIP_INSTALL_PATH/etc/rc.conf ]
	then
		verbose "rc.conf file for $SHIP_NAME found" 9
		if grep 'rc_configured=YES' $SHIP_INSTALL_PATH/etc/rc.conf >> /dev/null
		then
			verbose "Ship $SHIP_NAME is rc configured" 9
			for SERVICE_NAME in $(grep YES $SHIP_INSTALL_PATH/etc/rc.conf | egrep -v '#|rc_configured' | cut -d '=' -f1)
			do
				verbose "SERVICE_NAME $SERVICE_NAME is configured" 9 
				verbose "Trying to $ACTION service $SERVICE_NAME" 2
				chroot $SHIP_INSTALL_PATH /etc/rc.d/$SERVICE_NAME $ACTION
			done
		else
			verbose "Ship $SHIP_NAME is not rc configured" 9
		fi
	else
		verbose "rc.conf for $SHIP_NAME not found" 9
	fi	
}
validate_net_interface()
{
	local NET_TO_VALIDATE=$1	
	ifconfig $NET_TO_VALIDATE 2>1 >> /dev/null
	if [ $? -eq 1 ]
	then
		verbose "Unkown net interface $NET_TO_VALIDATE" 1
		exit 1
	fi
}
validate_ip_format()
{
	local ip=$1
    	if expr "$ip" : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$' >/dev/null
	then
        	for i in 1 2 3 4; do
            	if [ $(echo "$ip" | cut -d. -f$i) -gt 255 ]; then
                	return 1
            		fi
        	done
        	return 0
    	else
        	return 1
    	fi

}

exit_if_file_not_found()
{
	local FILE_TO_VALIDATE=$1
	#FILE_TO_VALIDATE=$1
	if ! [ -f $FILE_TO_VALIDATE ]
	then
		verbose "ERROR : file $FILE_TO_VALIDATE not present" 1
		exit 1
	fi
}
validate_file_presence()
{
	local FILE_TO_VALIDATE=$1
	if [ -f $FILE_TO_VALIDATE ]
	then
		return 0
	else
		return 1
	fi
}
validate_object_presence_in_state_file()
{
	local OBJECT_NAME=$1
	local STATUS_FILE=$2
	local OBJECT_TYPE=$3
	if ! grep $OBJECT_NAME $STATUS_FILE >> /dev/null
	then
		verbose "Error : $OBJECT_TYPE $OBJECT_NAME not found in $STATUS_FILE file" 1
		exit 1
	fi

}
validate_name_not_taken()
{
	local NAME_TO_VALIDATE=$1
	local FILE_TO_VALIDATE=$2
	if grep $NAME_TO_VALIDATE $FILE_TO_VALIDATE >> /dev/null
	then
		#echo "Name $NAME_TO_VALIDATE already taken."
		echo 1	
	fi
}
validate_hull_exist()
{
	local HULL_NAME=$1
	local HULL_STATUS_FILE=$2
	local HULL_EXIST=0
	for LINE in $(cat $HULL_STATUS_FILE)
	do
		local HULL_NAME_IN_STATUS_FILE="$(echo $LINE | cut -d ',' -f 1)"
		if [ "$HULL_NAME_IN_STATUS_FILE" == "$HULL_NAME" ]
		then
			HULL_EXIST=1
		fi	
	done
	if [ "$HULL_EXIST" -eq 1 ]
	then
		echo 1
	else
		echo 0
	fi
}
validate_path_exist()
{
        local PATH_TO_VALIDATE=$1
	local NAME_TO_VALIDATE=$2
	# Validating if path exists 
	if ! [ -d $PATH_TO_VALIDATE ]
	then
		verbose "Trying to create $PATH_TO_VALIDATE" 2
		mkdir -p $PATH_TO_VALIDATE >> /dev/null
		if ! [ -d $PATH_TO_VALIDATE ]
		then
			verbose "Error : Failed to create $PATH_TO_VALIDATE" 1
			exit 1
		fi
	fi
	verbose "Created $PATH_TO_VALIDATE" 2
}
validate_path_not_taken()
{
	local PATH_TO_VALIDATE=$1
	local NAME_TO_VALIDATE=$2
	local STATUS_FILE=$3
        # Validating if path is already in use with in status file
        if grep "$PATH_TO_VALIDATE/$NAME_TO_VALIDATE" $STATUS_FILE >> /dev/null
        then
                verbose "PATH $PATH_TO_VALIDATE already taken." 1
                exit 1
        fi
}
validate_netbsd_version()
{
	local VERSION_TO_VALIDATE=$1
	local IS_A_VALID_VERSION=0
	for VERSION_TO_COMPARE in $VALID_NETBSD_VERSIONS
	do
		if [ "$VERSION_TO_COMPARE" == "$VERSION_TO_VALIDATE" ]
		then
			IS_A_VALID_VERSION=1	
		fi
	done
	if [ $IS_A_VALID_VERSION -ne 1 ]
	then
		#verbose "Invalid version $VERSION_TO_VALIDATE" 1
		#exit 1
		echo 1
	fi
}
fail_if_path_not_exist()
{
	local PATH_TO_VALIDATE=$1
	if ! [ -d $PATH_TO_VALIDATE ]
	then
		verbose "Error : Path $PATH_TO_VALIDATE does not exist" 1
	fi
}
fail_if_no_more_args()
{
	ARG_REMAINING=$1
	OBJECT_TYPE=$2
	if [ "$ARG_REMAINING" -eq 0 ]
	then
		verbose "ERROR : You need to specify a $OBJECT_TYPE name" 1
		echo ""
		usage
	fi
}
download_all_tarballs()
{
	local NETBSD_VERSION=$1
	if [ -z "$NETBSD_VERSION" ]
	then
		NETBSD_VERSION="$(uname -r)"
		verbose "Assuming native NetBSD Version $(uname -r)" 1
		verbose "If you another NetBSD version than the native please specify one" 2
	fi
	validate_netbsd_version $NETBSD_VERSION
	if ! [ -d $CACHE_DIR/NetBSD-$NETBSD_VERSION/$ARCHITECTURE ]
	then
		mkdir -p $CACHE_DIR/NetBSD-$NETBSD_VERSION/$ARCHITECTURE
	fi
	get_compress_format $NETBSD_VERSION
	for SET in $FULL_WITH_X_SETS_ARRAY 		
	do
		download_tarball $SET.$COMPRESS_FORMAT $NETBSD_VERSION
	done
}
download_tarball()
{
	local TARBALL=$1
	local NETBSD_VERSION=$2
	validate_path_exist $CACHE_DIR/NetBSD-$NETBSD_VERSION/$ARCHITECTURE
	cd $CACHE_DIR/NetBSD-$NETBSD_VERSION/$ARCHITECTURE
	verbose "Downloading $SET.$COMPRESS_FORMAT from $NETBSD_REPO_URL/NetBSD-$NETBSD_VERSION/$ARCHITECTURE/binary/sets" 2
	ftp $NETBSD_REPO_URL/NetBSD-$NETBSD_VERSION/$ARCHITECTURE/binary/sets/$SET.$COMPRESS_FORMAT >> /dev/null
}
drydock_checks()
{
	if [ "$OS_NAME" != "NetBSD" ]
	then
		verbose "Error: Unsupported OS. Captain has been designed only for NetBSD." 1
		verbose "Take a look here : https://www.netbsd.org/ " 2
		exit 1
	fi
	if ! [ -f $HULL_STATUS_FILE ]
	then
		verbose "$HULL_STATUS_FILE not created. Trying to create it" 2
		touch $HULL_STATUS_FILE
	fi
	if ! [ -f $SHIP_STATUS_FILE ]
	then
		verbose "$SHIP_STATUS_FILE not created. Trying to create it" 2
		touch $SHIP_STATUS_FILE
	fi
}
ask_user_yes_no()
{
	QUESTION=$1
	ANSWER=""
	while [ "$ANSWER" != "Y" ] && [ "$ANSWER" != "y" ] && [ "$ANSWER" != "N" ] && [ "$ANSWER" != "n" ]
        do
                echo -n "$QUESTION"
                read ANSWER
		if [ "$ANSWER" != "Y" ] && [ "$ANSWER" != "y" ] && [ "$ANSWER" != "N" ] && [ "$ANSWER" != "n" ]
		then
			verbose "Wrong answer.. Please answer by Y/y ou N/n" 1
		fi
        done
}
verbose()
{
	OUTPUT_STRING=$1
	STRING_VERBOSE_LEVEL=$2
	#VERBOSE_LEVEL = 0 => Silent 
	#VERBOSE_LEVEL = 1 => Minimal (Shows what's going on)
	#VERBOSE_LEVEL = 2 => Standard (Shows what's going on and somehat how it's done)
	#VERBOSE_LEVEL = 3 => Verbose (Will even outpout some hints)
	#VERBOSE_LEVEL = 4,5,6,7,8 => Not used for now
	#VERBOSE_LEVEL = 9 => Debug
	
	if [ $STRING_VERBOSE_LEVEL == "" ] || [ -z $STRING_VERBOSE_LEVEL ]
	then
		STRING_VERBOSE_LEVEL=1
	fi
	if [ $STRING_VERBOSE_LEVEL -le $VERBOSE_LEVEL ]
	then
		case $STRING_VERBOSE_LEVEL in
			1)
				echo "$OUTPUT_STRING"
				;;
			2)
				echo "== $OUTPUT_STRING =="
				;;
			3)
				echo "++ $OUTPUT_STRING ++"
				;;
			9)
				echo "** $OUTPUT_STRING **"
				;;
			*)
				echo "$OUTPUT_STRING"
		esac

	fi
}
if [[ "$#" -eq 0 ]]
then
	usage
fi
drydock_checks
while (( "$#" ))
do
	case $1 in
		hull)
			shift
				case $1 in
				blueprint|wizard)
					#TODO remove name question if provided in stdin
					#shift
					hull_config_wizard
					;;
				build)
					# Build Hull
					shift
					fail_if_no_more_args "$#" "hull"
					build_hull $1 
					;;
				delete)
					# Delete conf
					shift
					fail_if_no_more_args "$#" "hull"
					delete_hull $1
					;;
				destroy)
					# Destroy hull 
					shift
					fail_if_no_more_args "$#" "hull"
					destroy_hull $1
					;;
				ls|list)
					list_hull
					;;
				download-sets|cache-warm)
					shift
					download_all_tarballs "$1"	
					;;
				flush-sets|cache-flush)
					rm -rf $CACHE_DIR/*	
					;;
				*)
					usage
					;;
				esac
			shift
			;;
		ship)
			shift
				case $1 in
				start)
					shift
					fail_if_no_more_args "$#" "ship"
					start_ship $1
					;;
				stop)
					shift
					fail_if_no_more_args "$#" "ship"	
					stop_ship $1
					;;
				blueprint|wizard)
					#shift
					ship_config_wizard	
					;;
				build)
					shift
					fail_if_no_more_args "$#" "ship"
					build_ship $1
					;;
				delete)
					shift
					fail_if_no_more_args "$#" "ship"
					delete_ship $1
					;;
				destroy)
					shift
					fail_if_no_more_args "$#" "ship"
					destroy_ship $1
					;;
				ls|list)
					list_ship
					;;
				ll)
					long_list_ship
					;;
				reset-running)
					reset_running_ship_status
					;;
				enter|board)
					shift
					fail_if_no_more_args "$#" "ship"
					board_ship $1
					;;
				execute|radio)
					shift
					fail_if_no_more_args "$#" "ship"
					SHIP_NAME=$1
					shift
					fail_if_no_more_args "$#" "ship"
					COMMAND="$@"	
					radio_ship $SHIP_NAME "$COMMAND"
					shift $#
					;;
				convert|strip)
					shift
					fail_if_no_more_args "$#" "ship"
					strip_ship $1
					;;
				*)
					usage
					;;
				esac
			if [ "$#" -gt 0 ]
			then
				shift
			fi
			;;
		*)
			usage
			;;
	esac
done
